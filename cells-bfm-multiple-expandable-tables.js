{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-multiple-expandable-tables>` Description.

    Example:

    ```html
    <cells-bfm-multiple-expandable-tables></cells-bfm-multiple-expandable-tables>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-multiple-expandable-tables | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmMultipleExpandableTables extends Polymer.Element {

    static get is() {
      return 'cells-bfm-multiple-expandable-tables';
    }

    static get properties() {
      return {
        title: String,
        text: String,
        amount: {
          type: Object,
          value: () => ({})
        },
        button: {
          type: Object,
          value: () => ({})
        },
        items: {
          type: Array,
          value: () => ([])
        },
        _icon: {
          type: String,
          value: 'coronita:expand'
        },
        iconClosed: {
          type: String,
          value: 'coronita:expand'
        },
        iconOpened: {
          type: String,
          value: 'coronita:bullet2'
        }
      };
    }
    _handleIcon(e) {
      let currentIcon = (e.detail.value) ? this.iconOpened : this.iconClosed;
      this.set('_icon', currentIcon);
    }

  }

  customElements.define(CellsBfmMultipleExpandableTables.is, CellsBfmMultipleExpandableTables);
}